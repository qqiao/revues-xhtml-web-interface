/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dalloz.webinterface;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;

/**
 *
 * @author QIAO
 */
@WebServlet(name = "JurisServlet", urlPatterns = {"/GetSimpleHtmlFromDateNum"})
public class JurisServlet extends FondServlet {

    private static final String XSL_JURIS = PROPERTIES_LOADER.get("juris.xsl_xml_to_html");
    private static final Logger LOGGER = Logger.getLogger("com.dalloz.webinterface.RevuesServlet");

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String date = request.getParameter("date");
        String nume = request.getParameter("nume");
        String format = request.getParameter("format");
        String idDateNum = date + "_" + nume;

        // Exporter XML
        String xml = databaseManager.getJurisXml(idDateNum);
        // Créer XML
        if (xml.trim().length() > 0) {
            if (format == null || format.equals("xhtml")) {
                File xmlFile = createXml("juris", idDateNum, xml);
                // transformation en XHTML
                Source xsltSource = new StreamSource(getServletContext().getRealPath(XSL_JURIS));
                try {
                    transformer = transFact.newTransformer(xsltSource);
                    String xhtmlDocument = xslTransformer.transform(xmlFile, transformer);

                    // afficher XHTML
                    showDocument(response, xhtmlDocument, idDateNum, format);
                } catch (TransformerConfigurationException e) {
                    LOGGER.log(Level.WARNING, e.toString());
                } catch (TransformerException e) {
                    LOGGER.log(Level.WARNING, e.toString());
                }
            } else if (format != null && format.equals("xml")) {
                // afficher XML
                showDocument(response, xml, idDateNum, format);
            }
        } else {
            getMessage(response, "Le document XML n'existe pas dans la base de données.");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
