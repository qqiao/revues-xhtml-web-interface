/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dalloz.webinterface;

import com.dalloz.api.xml.transformation.XSLTransformer;
import com.dalloz.database.DatabaseManager;
import com.dalloz.properties.PropertiesLoader;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

/**
 *
 * @author QIAO
 */
public class FondServlet extends HttpServlet {

    protected static DatabaseManager databaseManager;
    protected static final PropertiesLoader PROPERTIES_LOADER = PropertiesLoader.getInstance();
    private static final String ENCODING_UTF8 = PROPERTIES_LOADER.get("encoding_utf8");
    private static final String FILE_DIR = PROPERTIES_LOADER.get("file_dir");
    private static final String SERVER_PATH = PROPERTIES_LOADER.get("server_path");
    private static final String DESTINATION_PATH = FILE_DIR + SERVER_PATH;
    private static final Logger LOGGER = Logger.getLogger("com.dalloz.webinterface.FondServlet");
    protected static XSLTransformer xslTransformer;
    protected static TransformerFactory transFact;
    protected static Transformer transformer;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        databaseManager = new DatabaseManager();
        databaseManager.getConnection();
        xslTransformer = new XSLTransformer();
        transFact = TransformerFactory.newInstance();
    }

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    public void destroy() {
        databaseManager.closeConnection();
    }

    private boolean buildDirectory(File directory) {
        boolean success = false;

        if (directory.exists()) {
            if (!directory.canWrite() || !directory.canRead()) {
                LOGGER.log(Level.WARNING, "Le répertoire {0} n''est pas accessible en lecture / écriture", directory.getAbsolutePath());
            } else {
                success = true;
            }
        } else if (!directory.mkdirs()) {
            LOGGER.log(Level.WARNING, "Le répertoire {0} n''a pu être crée", directory.getAbsolutePath());
        } else {
            success = true;
        }

        return success;
    }

    protected File createXml(String type, String id, String xml) {
        File dirFond = new File(DESTINATION_PATH + File.separator + type);
        File xmlDir = null, xmlFile = null;

        if (buildDirectory(dirFond)) {
            if (type.equals("revues")) {
                xmlDir = buildDirRevues(dirFond, id, "XML");
            } else if (type.equals("juris")) {
                xmlDir = buildDirJuris(dirFond, id, "XML");
            }

            if (buildDirectory(xmlDir)) {
                xmlFile = new File(xmlDir, "document.xml");
                writeFile(xmlFile, xml);
            }
        }

        return xmlFile;
    }

    private File buildDirRevues(File destinationPath, String id, String typeDir) {
        File file = null;
        String base, chron, zann, folio;

        if ((id != null && id.trim().length() > 0)) {
            base = id.split("/")[0];
            chron = id.split("/")[1];
            zann = id.split("/")[2];
            folio = id.split("/")[3];

            file = new File(new File(new File(new File(new File(destinationPath, base), chron), zann), folio), typeDir);
        }

        return file;
    }

    private File buildDirJuris(File destinationPath, String id, String typeDir) {
        File file;
        String year = "", month = "", day = "", number;

        if ((id != null && id.trim().length() > 10 && id.indexOf("_") == 10) || id.indexOf("_") == 0) {
            if (id.indexOf("_") == 10) {
                year = id.substring(0, 4);
                month = id.substring(5, 7);
                day = id.substring(8, 10);
            } else if (id.indexOf("_") == 0) {
                year = "0000";
                month = "00";
                day = "00";
            }

            number = id.substring(id.indexOf("_") + 1, id.length());
            if (number.trim().length() == 0) {
                number = "XX-XX.XXX";
            }

            number = new StringBuilder(number.replaceAll("/", "-")).toString();

            if (number.indexOf(".") >= 0) {
                number = number.substring(0, number.indexOf(".")) + "@" + number.substring(number.indexOf(".") + 1, number.length());
            }

            if (number.indexOf(":") >= 0) {
                number = number.split(":")[0] + number.split(":")[1];
            }

            file = new File(new File(new File(new File(new File(destinationPath, year), month), day), number), typeDir);
        } else {
            file = new File(new File(destinationPath, id), typeDir);
        }

        return file;
    }

    private void writeFile(File file, String content) {
        try {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.getPath()), ENCODING_UTF8));
            bw.write(content);

            bw.close();
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Error: ", e);
        }
    }

    protected void getMessage(HttpServletResponse response, String message) throws IOException {
        response.setContentType("text/xml;charset=utf-8");
        PrintWriter out = response.getWriter();
        out.print("<ERROR>" + message + "</ERROR>");
        
        out.close();
    }

    protected void showDocument(HttpServletResponse response, String document, String id, String format) {
        try {
            PrintWriter out = response.getWriter();
            if (format != null && format.equals("xml")) {
                response.setContentType("text/xml;charset=UTF-8");
            }
            
            if (document != null && document.length() > 0) {
                out.print(document);
            } else {
                getMessage(response, "Le document " + id + " n'existe pas.");
            }
            out.close();
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, e.toString());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}