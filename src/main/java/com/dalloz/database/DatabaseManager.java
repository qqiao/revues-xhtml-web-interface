/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dalloz.database;

import com.dalloz.properties.PropertiesLoader;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author QIAO
 */
public class DatabaseManager {

    private static final PropertiesLoader PROPERTIES_LOADER = PropertiesLoader.getInstance();
    private static final String XML_DECLARATION = PROPERTIES_LOADER.get("xml_declaration");
    private static final String POSTGRESQL_URL_JURIS = PROPERTIES_LOADER.get("postgresql_url_juris");
    private static final String POSTGRESQL_USER_JURIS = PROPERTIES_LOADER.get("postgresql_user_juris");
    private static final String PASSWORD = PROPERTIES_LOADER.get("password");
    private static final String POSTGRESQL_URL_REVUES = PROPERTIES_LOADER.get("postgresql_url_revues");
    private static final String POSTGRESQL_USER_REVUES = PROPERTIES_LOADER.get("postgresql_user_revues");
    private static final Logger LOGGER = Logger.getLogger("com.dalloz.database.DatabaseManager");
    private static Connection connectionJuris, connectionRevues;

    // Méthode pour obtenir la connection à la base de données
    public void getConnection() {
        try {
            Class.forName("org.postgresql.Driver");
            connectionJuris = DriverManager.getConnection(POSTGRESQL_URL_JURIS, POSTGRESQL_USER_JURIS, PASSWORD);
            connectionRevues = DriverManager.getConnection(POSTGRESQL_URL_REVUES, POSTGRESQL_USER_REVUES, PASSWORD);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, e.toString());
        }
    }

    // Méthode pour fermer la connection avec la base de données
    public void closeConnection() {
        try {
            if (connectionJuris == null || connectionRevues == null) {
                return;
            }
            connectionJuris.close();
            connectionRevues.close();
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, e.toString());
        } finally {
            try {
                connectionJuris.close();
                connectionRevues.close();
            } catch (Exception e) {
                LOGGER.log(Level.WARNING, e.toString());
            }
        }
    }

    public String[] getRevuesXml(String id) {
        String[] results = {"", ""};
        SQLXML sqlxml;
        String type = "";

        if (connectionRevues != null) {
            String select = "select dtd_document from ied.documents where id='" + id + "'";

            try {
                PreparedStatement documentSelectStatement = connectionRevues.prepareStatement(select);
                ResultSet documentResultSet = documentSelectStatement.executeQuery();
                documentResultSet.next();

                results[0] = documentResultSet.getString("dtd_document");

                documentResultSet.close();
                documentSelectStatement.close();

                if (results[0].equals("D")) {
                    type = "doctrine";
                } else if (results[0].equals("J")) {
                    type = "juris";
                }
                select = "select " + type + " from ied.documents where id='" + id + "'";
                documentSelectStatement = connectionRevues.prepareStatement(select);
                documentResultSet = documentSelectStatement.executeQuery();
                documentResultSet.next();
                sqlxml = documentResultSet.getSQLXML(type);
                results[1] = sqlxml.getString();

                // Ajout de la déclaration au document XML
                results[1] = XML_DECLARATION + results[1];

                documentResultSet.close();
                documentSelectStatement.close();
            } catch (SQLException e) {
                LOGGER.log(Level.WARNING, e.toString());
            }
        }

        return results;
    }

    public String getJurisXml(String idDateNum) {
        String xml = "";

        if (connectionJuris != null) {
            String select = "select document from ied.jurisprudence_internet where id_date_num='" + idDateNum + "'";

            try {
                PreparedStatement documentSelectStatement = connectionJuris.prepareStatement(select);
                ResultSet documentResultSet = documentSelectStatement.executeQuery();
                documentResultSet.next();

                SQLXML sqlxml = documentResultSet.getSQLXML("document");
                xml = sqlxml.getString();

                documentResultSet.close();
                documentSelectStatement.close();

                // Ajout de la déclaration au document XML
                xml = XML_DECLARATION + xml;
            } catch (SQLException e) {
                LOGGER.log(Level.WARNING, e.toString());
            }
        }

        return xml;
    }
}
