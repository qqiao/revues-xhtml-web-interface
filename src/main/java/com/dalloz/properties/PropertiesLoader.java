/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dalloz.properties;

import java.util.ResourceBundle;

/**
 *
 * @author QIAO
 */
public class PropertiesLoader {

    private static final PropertiesLoader loader = new PropertiesLoader();
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("source");

    private PropertiesLoader() {
    }

    public static PropertiesLoader getInstance() {
        return PropertiesLoader.loader;
    }

    public static PropertiesLoader getInstance(String bundleName) {
        resourceBundle = ResourceBundle.getBundle(bundleName);
        return PropertiesLoader.loader;
    }

    public String get(String key) {
        return resourceBundle.getString(key).trim();
    }
}