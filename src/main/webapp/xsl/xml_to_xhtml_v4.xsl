<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:xlink="http://www.w3.org/1999/xlink" exclude-result-prefixes="xlink">
	<xsl:output encoding="UTF-8" method="xhtml" indent="yes" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>

	<!-- ++++++++++ ++++++++++ ++++++++++ ++++++++++ ++++++++++ -->
	<!-- START -->
	<!-- ++++++++++ ++++++++++ ++++++++++ ++++++++++ ++++++++++ -->
	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>

	<!-- ++++++++++ ++++++++++ ++++++++++ ++++++++++ ++++++++++ -->
	<!-- SUPPRESSION -->
	<!-- ++++++++++ ++++++++++ ++++++++++ ++++++++++ ++++++++++ -->
	<xsl:template match="comment() | processing-instruction()"/>

	<!-- ++++++++++ ++++++++++ ++++++++++ ++++++++++ ++++++++++ -->
	<!-- COPIE BRUTE -->
	<!-- ++++++++++ ++++++++++ ++++++++++ ++++++++++ ++++++++++ -->
	<xsl:template match="text()">
		<xsl:copy/>
	</xsl:template>

	<xsl:template match="*">
		<xsl:element name="{ local-name() }">
			<xsl:apply-templates select="@*|node()"/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="@*">
		<xsl:attribute name="{ name() }">
			<xsl:value-of select="."/>
		</xsl:attribute>
	</xsl:template>

	<!-- ++++++++++ ++++++++++ ++++++++++ ++++++++++ ++++++++++ -->
	<!-- ELEMENTS DE TYPO -->
	<!-- ++++++++++ ++++++++++ ++++++++++ ++++++++++ ++++++++++ -->
	<!-- <!ELEMENT BR EMPTY> -->
	<!-- %inline  -->
	<xsl:template match="BR">
		<br/>
	</xsl:template>

	<xsl:template match="GRAS">
		<br/>
		<br/>
		<b>
			<xsl:apply-templates/>
		</b>
	</xsl:template>

	<xsl:template match="ITAL">
		<i>
			<xsl:apply-templates/>
		</i>
	</xsl:template>

	<xsl:template match="EXP">
		<sup>
			<xsl:apply-templates/>
		</sup>
	</xsl:template>

	<xsl:template match="P">
		<p>
			<xsl:apply-templates/>
		</p>
	</xsl:template>

	<!-- ++++++++++ ++++++++++ ++++++++++ ++++++++++ ++++++++++ -->
	<!-- ELEMENTS PRINCIPAUX, UNITÉ DOCUMENTAIRE ITEMJUR -->
	<!-- ++++++++++ ++++++++++ ++++++++++ ++++++++++ ++++++++++ -->
	<!-- <!ELEMENT ITEMJUR (META, DOCUMENT) > -->
	<!-- <!ATTLIST ITEMJUR xmlns:xlink CDATA #IMPLIED > -->
	<!-- ITEMJUR  -->

	<xsl:template match="JURIS">
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="ITEMJUR">
		<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
				<title>
					<xsl:apply-templates select="TITREVUE"/>
				</title>
				<style type="text/css">
					/* juris.css */

/* **********************
 *	Styles communs		*
 ************************/
 
/* TAGS par défaut */
	body,td,th,input,select { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; }
	h3{ margin:0; padding:0; }
	body, p, form, h3, dl, dt, dd, select, imput, ul, li { margin:0; padding:0; }
	body { margin: 0px 0px 0px 0px; background-color:#FFFFFF; }

p { margin:10px; }
h1, h2, h3, h4, h5, h6 { font-size:1.0em; }

/* styles italiques et gras */
	.ital { font-style:italic; }
	.gras { font-weight:bold; }

/* date féré à droite (commentaire,...) */
	.date { text-align: right; vertical-align: top; padding: 3px 10px 0px 0px; }

/* Copyright (commentaire,...)*/
	.copyright { color: #BB1126; text-align: center; padding-top: 10px;}
	
/* Copyright dans la navigation en bas (liste de résultats/résultat/documents/popup)*/
	.copyrightInNav { color: #BB1126; text-align: right; padding-right: 10px;}

/*div permettant de marger tous les conteneurs (utiliser pour les navigations, conteus, etc.)*/ 
/* .divFullMarge { padding-right: 10px; padding-left:10px; } */
/*	* html .divFullMarge { width:100%; padding: 0px 9px 0px 10px; }*/

/* Style appliqué aux tables conteneur (qui contiennent généralement la totalité du contenu) afin qu'ils soient margés */
	.mainTableFormat { margin-left: 5px; width: 97%; }
	
/*id permettant de marquer certaines tag en fond blanc*/
	#fBlanc { background-color:#FFFFFF; }
	
/*id permettant de marquer certains tag en texte normal*/
	#tNormal { font-weight:normal; }
	
/* Bloc fonctionnel imprimer/Augmenter/Diminuer le commentaire	*/
/* #blocImprimerTypo { float:right; position:absolute; right:204px; top:13px; } */

/* **********
*	liens	*
*************/
/*  générique  */
	a:link,a:active,a:visited { color: #000000; font-weight: bold; text-decoration: none; } /* Communs à toutes les balises a. */
	a:hover { color: #BB1126; text-decoration: underline; }
	
	a.mail:link, a.mail:active, a.mail:visited { text-decoration: underline; }  /* Communs à toutes les adresses mail */
	a.url:link, a.url:active, a.url:visited  { text-decoration: underline; }  /* Communs à toutes les liens url */
	
/* Liens page précédente | page suivante */
	#pageSuivPrec { color:#BB1126; text-align:center; padding:15px 0; }
	#pageSuivPrec a:link, #pageSuivPrec a:visited, #pageSuivPrec a:hover, #pageSuivPrec a:active { color:#BB1126; font-weight:normal; text-decoration:underline; }
	#pageSuivPrec a:hover { text-decoration:none; }


/* Lien a dans un document actualités */
a.lienaInActu {color:Blue}
a.lienaInActu a:hover{text-decoration:underline}

/* style.css */


/* **********************************************************************
*	surcharge de la class se trouvant dans commun.css					*
*	--> commun à la liste de résultat et au document proprement dit		*
*************************************************************************/ 
	/* .divFullMarge { padding: 0px 27px 0px 10px; } */
	/** html .divFullMarge { width:100%; padding: 0px 9px 0px 10px; }*/ /*hack IE Mac et PC*/
	
/* **********************************************
 *	Résultat d'une recherche / document			*
 ************************************************/

/*
 *	Communs à tous les fonds			
 ************************************************/
  
/* tableau conteneur du commentaire du résultat pour tous les fonds et la liste de résultats*/
	.tabCommentDoc, .tabListResult, .tabCommentPopupNDC { width:100%; }

/* tableau conteneur du commentaire des popups Notes de bas de page / et documents liés*/
	/* .tabCommentPopupNDC {} Factorisé à .tabCommentDoc */
	.tabCommentPopupNDC { height:241px; background-position:left 25px; background-repeat:no-repeat; }

/* Référence du résultat --> ex : Recueil Dalloz 2002 Chroniques p. 1821 */
	.refDoc { /*font-size:12px;*/ font-size:1.1em; height:20px; font-weight:bold; padding: 5px 10px 4px 10px; background-color:#EFEFEF; }
	
/* td incluant le titre / le sous commentaire du titre / l'auteur du commentaire / etc. */
	.tdTitreDoc { /*font-size:12px;*/ font-size:1.1em; text-align: justify; padding: 10px 0px 5px 10px; border:3px solid #EFEFEF; background-color: #FBFBFB; }

/* commentaire du document */ 
	.commentDoc, .commentPopupNDC { /*font-size:11px;*/ text-align: justify; padding: 10px 0px 20px 10px; }
	
/* RESUFRAN */
/* '#&lt;DI> : 874 | &lt;SVN> : 2.19.01 | &lt;INT> : SGR | &lt;DATE> : 19/07/2007#' Suppression style italique */
#RESUFRAN	{ margin-bottom:10px; padding:10px; font-style:normal; background-color:#EFEFEF;}

#RESUFRAN span.titre	{ font-weight:bold; color:#BB1126; }

/* rapport du document */ 
	.rapportDoc { /*font-size:11px;*/ text-align: justify; padding: 10px 0px 20px 50px; }

/* commentaire d'un document des popups Notes de bas de page / et documents liés */ 
	/* .commentPopupNDC {} Factorisé à .commentDoc */
	.commentPopupNDC { padding-right:10px; }

/* Ajout d'une spécificité au refDoc de la popup documents liés */
	#refDocLies { border:1px solid #CCCCCC; }

/* Les occurences trouvés*/	
	.occurence { color:#000000; background-color:#FFFF00; }
	
/* Cellule représentant le début du commentaire (carré 3 étoiles en images) */
	.debutComment { /*font-size:11px;*/ height: 27px; background-image:url(../../commun/img/debutComment.gif); background-repeat:no-repeat; background-position:center 15px; }
	.stars { height:27px; background:url(../../images/entete.gif) no-repeat center; }
	
/* Cellule représentant la fin du document (carré)  */
	.cellFinDoc { height: 30px; background-image:url(../../commun/img/puceC69B39.gif); background-repeat:no-repeat; background-position:center center; text-align:right; }

/* Gestion des icônes dans le document */
	#icoDoc { margin:0px 3px 0px 3px; }
	
/* Cellule conteneur des notes de bas de page de la POPUP D'IMPRESSION */
	/* .notesImpression { color:#70707A; margin-top:20px; padding: 5px 5px 5px 5px; border:1px dotted #BFBFBF; } */
	
/* Cellule représentant les points */
	.point {}

/* terme sommaire */
	#sommaireTxt, #txtIntegralTxt { font-weight:bold; color:#BB1126; }

/* terme sommaire */
	/* #txtIntegralTxt {} --> factorisé à #sommaireTxt */

/*
 *	Spécifiques aux fonds :
 ************************************************/
/* Chroniques */

	/* titre */	
		.chronTITRE, .chronDVLPMTTITRE, .jurisJURI, .jurisCHAM, .legisSOUR { /*font-size:11px;*/ font-weight:bold; color:#BB1126; }
		
	/* sous titre */	
		.chronSTITRE, .jurisDATE, .jurisSOMMAIRE, .legisINTT { /*font-size:11px;*/ font-weight:bold; }
	
	/* signature(auteur) */	
		.chronSIGNATURE { font-weight:bold; color:#555555; }
	
	/* premier et seul niveau de titre */	
		.chronDVLPMTTITRE { /*font-size:12px;*/ font-size:1.1em; } /* + Factorisé à .chronTITRE */

/* JurisPrudence */

	/* juridiction*/
	/* .jurisJURI {} Factorisé à .chronTITRE */
	
	/* chambre */
	/* .jurisCHAM {} Factorisé à .chronTITRE */
	
	/* date */
	/* .jurisDATE {} Factorisé à .chronSTITRE */
	
	/* ex:n° 99-15.696(n° 804 FS-P) */
	.jurisNAAF { /*font-size:11px;*/ } /* style par défaut */
	
	/* Sommaire (le texte) */
	.jurisSOMMAIRE, .legisVISA { /*font-size:11px;*/ text-align: justify; padding: 10px 0px 0px 10px; color:#6A6A6A; } /* + Factorisé à .chronSTITRE */
	
	/* Décision en bas de page (encadrée) */
	.jurisDECIBOTTOM { /*font-size:11px;*/ margin-top:20px; padding:5px 5px 5px 5px; border: 1px dotted #C8C8CC; }

/* Législation */

	/* source */
	/* .legisSOUR {} Factorisé à .chronTITRE */
	
	/* intitulé */
	/* .legisINTT {} Factorisé à .chronSTITRE */
	
	/* ex: JOCE L 200, 8 août 2000, p35 */
	.legisJOOF { /*font-size:11px;*/ } /* style par défaut */
	
	/* ex: NOR : FAMX8900027L */
	.legisZNOR { /*font-size:11px;*/ } /* style par défaut */
	
	/* visa : vue le, vu le... */
	.legisVISA { /*font-size:11px;*/ color:#BB1126; } /* + Factorisé à .jurisSOMMAIRE */
	
	/* numéro d'article */
	.legisNUMA { /*font-size:11px;*/ font-weight:bold; color:#BB1126; }
	
	.biblioINTIT { /*font-size:12px;*/ font-size:1.1em; font-weight:bold; color:#BB1126; }
	
	/* titre d'article */
	.legisTITART { /*font-size:11px;*/ font-weight:bold; }
	
	/* gestion des cinq niveaux de titres */
	.legisTITUN, .legisTITDEUX, .legisTITTROIS, .legisTITQUATRE, .legisTITCINQ { text-align:center; }
		.legisTITUN, .legisTITDEUX  { /*font-size:12px;*/ font-size:1.1em; font-weight:bold; }
		.legisTITDEUX { font-weight:normal; }
		.legisTITTROIS, .legisTITQUATRE { /*font-size:11px;*/ font-weight:bold; color:#555555; }
		.legisTITQUATRE { font-weight:normal; }
		.legisTITCINQ { /*font-size:11px;*/ color:#666666; } /* style par défaut */

/* Biblio */
 	/* les intulés  */
	/* .biblioINTIT {} Factorisé à .legisNUMA */

/* ******************************************************
 *	DALLOZ JURISPRUDENCE								*
 ********************************************************/
	
	/* Citation (liens inverses) de la jurisprudence */
	#CITATION
	{
		padding:10px; margin:20px 0 30px 0;
		border:2px solid #D9D9D9;
	}
	#CITATION h5,
	#CITATION h6	{ font-size:100%; margin:0; padding:0; text-transform:uppercase; }
	#CITATION h5 	{ margin-bottom:10px; color:#BB1126; border-bottom:2px solid #E4E4E4; }
	#CITATION h6 	{ font-weight:normal; margin:10px 0 3px 0; }
	
	#CITATION ul, 
	#CITATION li 	{ margin:0; padding:0; }
	#CITATION li 	{ margin-bottom:5px; padding-left:15px; list-style:none; background:transparent url(../../images/puce.gif) no-repeat 1px 2px; }
	
	#CITATION a:link,
	#CITATION a:visited,
	#CITATION a:hover,
	#CITATION a:active
	{
		font-weight:normal; text-decoration:none; color:#BB1126;
		border-bottom:1px solid #BB1126;
	}

	/* Entete du texte intégral (type legifrance) */
	#texteIntegral-entete				{ padding:0 0 20px 0; line-height:170%; }
	#texteIntegral-entete span 			{ display:block; font-weight:bold; }
	#texteIntegral-entete #deci-juri	{}
	#texteIntegral-entete #deci-cham	{}
	#texteIntegral-entete #deci-date	{}
	#texteIntegral-entete #deci-sens	{ float:right; }
	#texteIntegral-entete #deci-naff	{ padding-top:20px; }
	#texteIntegral-entete #pub			{ font-weight:normal; }
	
	/* Titres et libellés propres aux arrêts Legifrance */
	#titre-legifrance-1,
	#titre-legifrance-2
	{
		margin:0; padding:0 0 25px 0;
		font-size:1em; text-transform:uppercase; text-align:center;
	}

/* ******************************************************
 *	Mots clés  											*
 ********************************************************/

/* terme mots clés du bloc mot clés*/	
	#motsClesTxt { font-weight:bold; }
		
/*bloc (td) de mots clés */
	.motsCles { /*font-size:11px;*/ text-align: justify; padding: 4px 10px 4px 10px; background-color:#F3F3F3; }
	
/*Titre des mots clés figurant dans le bloc*/
	.titreMotsCles { /*font-size:11px;*/ font-weight:bold; color:#BB1126; text-transform: uppercase; }



				</style>
			</head>
			<body>
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="tdTitreDoc">
							<xsl:apply-templates select="RESUFRAN"/>
							<xsl:apply-templates select="DECI"/>
							<xsl:apply-templates select="PUB"/>
						</td>
					</tr>

					<xsl:apply-templates select="SOMMAIRE"/>

					<tr>
						<td class="stars"/>
					</tr>

					<xsl:if test="INTEGRAL/TITRE/text() | INTEGRAL/P/text() | INTEGRAL/P/P/text() | INTEGRAL/text()">
						<xsl:call-template name="TEXTE_INTEGRAL">
							<xsl:with-param name="intemjur" select="."/>
						</xsl:call-template>
						<tr>
							<td class="commentDoc">
								<xsl:if test="NO">
									<xsl:if test="TYPE[text() = 'CASS'] | TYPE[text() = 'INCA'] | TYPE[text() = 'JADE']">
										<h4 style="text-align:center">
											<xsl:text>REPUBLIQUE FRANCAISE</xsl:text>
										</h4>
										<h4 style="text-align:center">
											<br/>
											<xsl:text>AU NOM DU PEUPLE FRANCAIS</xsl:text>
										</h4>
									</xsl:if>
								</xsl:if>
								<xsl:apply-templates select="INTEGRAL"/>
							</td>
						</tr>
					</xsl:if>
					<xsl:if test="DECI/PART/DEMA/text() | DECI/PART/DEFE/text() | DECI/COMPOSIT/text() | DECI/ATTA/JURIDIC/JURI/text()">
						<tr>
							<td>
								<br/>
								<br/>
							</td>
						</tr>
						<xsl:call-template name="BOTTOM">
							<xsl:with-param name="deci" select="DECI"/>
						</xsl:call-template>
					</xsl:if>
					<xsl:apply-templates select="* except (TITREVUE | DECI | PUB | LIENDOCT | SOMMAIRE | INTEGRAL | RESUFRAN | NOTES | IED)"/>
				</table>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="TITREVUE">
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="RESUFRAN">
		<xsl:if test="P/text() | P/P/text()">
			<div class="jurisJURI">
				<xsl:apply-templates/>
			</div>
			<br/>
		</xsl:if>
	</xsl:template>

	<xsl:template match="DECI">
		<xsl:apply-templates select="JURIDIC"/>
		<br/>
		<xsl:apply-templates select="DATE"/>
		<xsl:apply-templates select="NAFF"/>
		<xsl:apply-templates select="* except(JURIDIC | DATE |  ATTA/SENS | NAFF | ATTA | COMPOSIT)"/>
	</xsl:template>

	<xsl:template match="DECI/JURIDIC">
		<xsl:apply-templates select="JURI"/>
		<xsl:apply-templates select="CHAM"/>
		<xsl:apply-templates select="* except(JURI|CHAM)"/>
	</xsl:template>

	<xsl:template match="DECI/JURIDIC/JURI">
		<xsl:if test="text()">
			<div class="jurisJURI">
				<xsl:apply-templates/>
			</div>
		</xsl:if>
	</xsl:template>

	<xsl:template match="CHAM">
		<xsl:if test="text()">
			<div class="jurisCHAM">
				<xsl:apply-templates/>
			</div>
		</xsl:if>
	</xsl:template>

	<xsl:template match="DECI/DATE">
		<xsl:if test="text()">
			<xsl:choose>
				<xsl:when test="string-length(substring-before(., '-')) > 0">
					<div class="jurisDATE">
						<xsl:call-template name="DATE">
							<xsl:with-param name="date" select="."/>
						</xsl:call-template>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<div>
						<b>
							<xsl:apply-templates/>
						</b>
					</div>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>

	<xsl:template match="ATTA">
		<xsl:apply-templates select="JURIDIC/JURI"/>
		<xsl:text>&#160;</xsl:text>
		<xsl:apply-templates select="DATE"/>
	</xsl:template>

	<xsl:template match="SENS">
		<div style="text-align:right">
			<b>
				<xsl:apply-templates/>
			</b>
		</div>
	</xsl:template>

	<xsl:template match="NAFF">
		<xsl:if test="text()">
			<div class="jurisNAAF">
				<xsl:text>n° </xsl:text>
				<xsl:apply-templates/>
			</div>
		</xsl:if>
	</xsl:template>

	<xsl:template match="ATTA/JURIDIC">
		<xsl:apply-templates select="JURI"/>
	</xsl:template>

	<xsl:template match="ATTA/JURIDIC/JURI">
		<xsl:text>Jugement attaqué : </xsl:text>
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="ATTA/DATE">
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="PUB">
		<xsl:if test="text()">
			<div>
				<i>
					<xsl:text>Publication : </xsl:text>
				</i>
				<xsl:apply-templates/>
			</div>
		</xsl:if>
	</xsl:template>

	<xsl:template match="LIENDOCT">
		<xsl:copy-of select="."/>
	</xsl:template>

	<xsl:template match="SOMMAIRE">
		<xsl:if test="P/text() | P/P/text()">
			<tr>
				<td class="jurisSOMMAIRE">
					<span style="color:#BB1126; font-weight:bold">Sommaire :</span>
					<xsl:apply-templates/>
				</td>
			</tr>
		</xsl:if>
	</xsl:template>

	<xsl:template match="COMPOSIT">
		<p>
			<b>
				<xsl:apply-templates/>
			</b>
		</p>
	</xsl:template>

	<xsl:template name="TEXTE_INTEGRAL">
		<xsl:param name="intemjur"/>
		<tr>
			<td class="commentDoc">
				<div>
					<span style="color:#BB1126;">
						<b>
							<xsl:text>Texte intégral :</xsl:text>
						</b>
					</span>
				</div>
				<br/>
				<xsl:if test="DECI/JURIDIC/JURI/text()">
					<div>
						<b>
							<xsl:value-of select="DECI/JURIDIC/JURI"/>
						</b>
					</div>
				</xsl:if>
				<xsl:if test="DECI/JURIDIC/CHAM/text()">
					<div>
						<b>
							<xsl:value-of select="DECI/JURIDIC/CHAM"/>
						</b>
					</div>
				</xsl:if>
				<xsl:apply-templates select="DECI/DATE"/>
				<xsl:apply-templates select="DECI/ATTA/SENS"/>
				<xsl:if test="DECI/NAFF/text()">
					<div>
						<b>
							<xsl:text>N° </xsl:text>
							<xsl:value-of select="DECI/NAFF"/>
						</b>
					</div>
				</xsl:if>
				<xsl:if test="PUB/text()">
					<div>
						<xsl:value-of select="PUB"/>
					</div>
				</xsl:if>
			</td>
		</tr>
	</xsl:template>

	<!--<xsl:template match="INTEGRAL">
		<xsl:for-each-group select="element()" group-adjacent="self::P or self::xxx">
			<xsl:choose>
				<xsl:when test="current-grouping-key()">
					<xsl:apply-templates select="current-group()"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="current-group()"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each-group>
		</xsl:template>-->

	<xsl:template match="INTEGRAL">
		<xsl:choose>
			<xsl:when test="P or xxx">
				<xsl:for-each-group select="element()" group-adjacent="self::P or self::xxx">
					<xsl:choose>
						<xsl:when test="current-grouping-key()">
							<xsl:apply-templates select="current-group()"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:apply-templates select="current-group()"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each-group>
			</xsl:when>
			<xsl:otherwise>
				<p>
					<xsl:apply-templates/>
				</p>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="P[ancestor::SOMMAIRE]">
		<br/>
		<br/>
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="P[parent::P and ancestor::INTEGRAL]">
		<br/>
		<br/>
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="P[ancestor::RESUFRAN]">
		<xsl:apply-templates/>
		<br/>
		<br/>
		<br/>
	</xsl:template>

	<xsl:template name="BOTTOM">
		<xsl:param name="deci"/>
		<xsl:variable name="dateAtta">
			<xsl:if test="$deci/ATTA/DATE/text()">
				<xsl:call-template name="DATE_ATTA">
					<xsl:with-param name="date" select="$deci/ATTA[1]/DATE"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:variable>

		<tr>
			<td>
				<div class="jurisDECIBOTTOM">
					<xsl:if test="$deci/PART/DEMA/text()">
						<b>
							<xsl:text>Demandeur : </xsl:text>
						</b>
						<xsl:value-of select="$deci/PART/DEMA"/>
						<br/>
					</xsl:if>
					<xsl:if test="$deci/PART/DEFE/text()">
						<b>
							<xsl:text>Défendeur : </xsl:text>
						</b>
						<xsl:value-of select="$deci/PART/DEFE"/>
						<br/>
					</xsl:if>
					<xsl:if test="$deci/COMPOSIT/text()">
						<b>
							<xsl:text>Composition de la juridiction : </xsl:text>
						</b>
						<xsl:value-of select="$deci/COMPOSIT"/>
						<br/>
					</xsl:if>
					<xsl:if test="$deci/ATTA/JURIDIC/JURI/text()">
						<b>
							<xsl:text>Décision attaquée : </xsl:text>
						</b>
						<xsl:value-of select="concat($deci/ATTA/JURIDIC/JURI, ' ', $dateAtta)"/>
						<xsl:if test="$deci/ATTA/SENS/text()">
							<xsl:value-of select="concat(' ', '(', $deci/ATTA/SENS, ')')"/>
						</xsl:if>
					</xsl:if>
				</div>
			</td>
		</tr>
	</xsl:template>

	<xsl:template name="DATE">
		<xsl:param name="date"/>

		<xsl:variable name="dateStr">
			<xsl:choose>
				<xsl:when test="substring($date, 1, 1) = '0'">
					<xsl:value-of select="substring($date, 2, string-length())"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$date"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="months" select="('janvier','février','mars','avril','mai','juin','juillet','août','septembre','octobre','novembre','décembre')"/>
		<xsl:value-of select="concat(tokenize($dateStr, '-')[1], ' ', $months[number(tokenize($dateStr, '-')[2])], ' ', tokenize($dateStr, '-')[3])"/>
	</xsl:template>

	<xsl:template name="DATE_ATTA">
		<xsl:param name="date"/>
		<xsl:variable name="months" select="('janvier','février','mars','avril','mai','juin','juillet','août','septembre','octobre','novembre','décembre')"/>

		<xsl:variable name="dateStr">
			<xsl:value-of select="concat(tokenize($date, '-')[3], ' ', $months[number(tokenize($date, '-')[2])], ' ', tokenize($date, '-')[1])"/>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="substring($dateStr, 1, 1) = '0'">
				<xsl:value-of select="substring($dateStr, 2, string-length())"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$dateStr"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="DATE-YYYYMMDD">
		<xsl:if test="empty(/JURIS/ITEMJUR/DECI/DATE)">
			<xsl:text>0000-00-00</xsl:text>
		</xsl:if>
		<xsl:if test="exists(/JURIS/ITEMJUR/DECI/DATE)">
			<xsl:variable name="date" select="/JURIS/ITEMJUR/DECI/DATE"/>
			<xsl:if test="string-length($date)=0">
				<xsl:text>0000-00-00</xsl:text>
			</xsl:if>
			<xsl:if test="string-length($date) &gt; 0">
				<xsl:choose>
					<xsl:when test="string-length(substring-after($date, '-')) > 0">
						<xsl:value-of select="concat(tokenize($date, '-')[3], '-', tokenize($date, '-')[2], '-', tokenize($date, '-')[1])"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="tokens" select="tokenize($date, ' ')"/>
						<xsl:variable name="day" select="$tokens[1]"/>
						<xsl:variable name="month" select="$tokens[2]"/>
						<xsl:variable name="year" select="$tokens[3]"/>
						<xsl:value-of select="$year"/>
						<xsl:value-of select="'-'"/>
						<xsl:choose>
							<xsl:when test="$month = 'janvier'">01</xsl:when>
							<xsl:when test="$month = 'février'">02</xsl:when>
							<xsl:when test="$month = 'mars'">03</xsl:when>
							<xsl:when test="$month = 'avril'">04</xsl:when>
							<xsl:when test="$month = 'mai'">05</xsl:when>
							<xsl:when test="$month = 'juin'">06</xsl:when>
							<xsl:when test="$month = 'juillet'">07</xsl:when>
							<xsl:when test="$month = 'août'">08</xsl:when>
							<xsl:when test="$month = 'septembre'">09</xsl:when>
							<xsl:when test="$month = 'octobre'">10</xsl:when>
							<xsl:when test="$month = 'novembre'">11</xsl:when>
							<xsl:when test="$month = 'décembre'">12</xsl:when>
						</xsl:choose>
						<xsl:value-of select="'-'"/>
						<xsl:if test="(string-length($day) &lt; 2)">
							<xsl:value-of select="0"/>
						</xsl:if>
						<xsl:value-of select="$day"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:if>
	</xsl:template>

	<xsl:template name="NAFF">
		<xsl:if test="empty(/JURIS/ITEMJUR/DECI/NAFF[1])">
			<xsl:text>XX-XX.XXX</xsl:text>
		</xsl:if>
		<xsl:if test="exists(/JURIS/ITEMJUR/DECI/NAFF[1])">
			<xsl:variable name="naff" select="/JURIS/ITEMJUR/DECI/NAFF[1]"/>
			<xsl:if test="string-length($naff)=0">
				<xsl:text>XX-XX.XXX</xsl:text>
			</xsl:if>
			<xsl:if test="string-length($naff) &gt; 0">
				<xsl:choose>
					<xsl:when test="string-length(substring-after($naff, ',')) > 0">
						<xsl:value-of select="tokenize($naff, ',')[1]"/>
					</xsl:when>
					<xsl:when test="string-length(substring-before($naff, '-(')) > 0">
						<xsl:value-of select="substring-before($naff, '-(')"/>
					</xsl:when>
					<xsl:when test="string-length(substring-before($naff, ' (')) > 0">
						<xsl:value-of select="substring-before($naff, ' (')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$naff"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:if>
	</xsl:template>

	<xsl:template match="NO | FOLI | ZANN | TYPE | INDEX | PART | THEME | FONDLEG | ANOTE"/>

</xsl:stylesheet>
